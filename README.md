**Meridian Group S.A.S. - Prueba técnica para frontend**

Instrucciones para llevar a cabo la prueba técnica de frontend, aplicable a los postulantes para el cargo de Desarrollador Frontend. 

*El tiempo límite para el desarrollo de esta prueba está fijado en 2 horas.*

---

## Objetivo de la prueba

Se busca evaluar las habilidades del candidato en los siguientes aspectos:

1. HTML
2. CSS
3. JavaScript
4. Angular
5. Implementación Responsive
6. Buenas prácticas en la escritura del código
7. Priorización de requerimientos
8. Creatividad y recursividad en la solución de problemas en un plazo fijo

---

## Ejericio a realizar

Siga cada uno de los pasos indicados a continuación para el desarrollo de la prueba

1. Clone este repositorio en su ambiente de desarrollo
2. Cree un branch llamado **<nombre>-<apellido>** reemplazando cada etiqueta con sus datos personales, publíquelo en el repositorio antes de empezar.
3. [Maquetación] Traduzca el diseño ubicado en **assets/Design PSD** a HTML+CSS. Si lo requiere, puede descargar el PSD ubicado en la carpeta. Debe utilizar técnicas CSS3 y HTML semántico cuando lo requiera. 
4. [Maquetación] El sitio debe ser implementado para desktop (mínimo 1366x768) y dispositivos móviles (teléfonos iPhone y Android). Se recomienda utilizar FontAwesome donde sea necesario.
5. [JavaScript] El sitio debe validar la marca del automóvil, modelo del automóvil, año del automóvil y el email del usuario (todos los campos son requeridos)
6. [SEO Friendly] Cree las etiquetas necesarias para un buen SEO (hint: use las keywords: crédito automotriz, comparador crédito automotriz). ¿Crees que se requieran cambios en la maqueta? ¿Cuáles? Opcional: agregar share buttons y etiquetas para redes sociales (hint: http://ogp.me).
7. [Advanced CSS] Puede usar frameworks a elección para escribir CSS teniendo en cuenta la compatibilidad con distintos browsers (hint: Usar BrowserStack para chequear el renderizado en distintos navegadores). Opcional: ¿cuál sería tu enfoque en la construcción del diseño?

---

## Finalización del ejercicio

Incluya en el Readme de su rama la siguiente información:

1. Aspectos técnicos importantes para la ejecución de su programa (consideraciones operacionales, requerimientos no funcionales, otros)
2. Solución a las dudas planteadas en los puntos 6 y 7 del ejercicio
3. Paso a paso para la ejecución de su proyecto en un ambiente local (servicios/aplicaciones, configuraciones, comentarios)
4. Recomendaciones para una siguiente versión de la aplicación.

Genere un pull request una vez se encuentre completa la prueba.